package com.bbva.czic.account.business.dto;

import java.util.List;

public class DTOIntAccount extends DTOIntContract {

	private List<DTOIntAccountBlocking> blockings;
	private List<DTOIntIndicator> indicators;
	public DTOIntAccount() {
	}
	
	
	public List<DTOIntIndicator> getIndicators() {
		return indicators;
	}


	public void setIndicators(List<DTOIntIndicator> indicators) {
		this.indicators = indicators;
	}


	public List<DTOIntAccountBlocking> getBlockings() {
		return blockings;
	}

	public void setBlockings(List<DTOIntAccountBlocking> blockings) {
		this.blockings = blockings;
	}
}
