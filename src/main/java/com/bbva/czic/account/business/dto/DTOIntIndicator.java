package com.bbva.czic.account.business.dto;

import java.io.Serializable;

public class DTOIntIndicator implements Serializable{
	
	public final static long serialVersionUID = 1L;
	private String isActive;
	private String indicatorId;
	private String name;
	
	public DTOIntIndicator(){
		
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIndicatorId() {
		return indicatorId;
	}

	public void setIndicatorId(String indicatorId) {
		this.indicatorId = indicatorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
