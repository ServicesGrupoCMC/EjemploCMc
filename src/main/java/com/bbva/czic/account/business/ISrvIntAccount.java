package com.bbva.czic.account.business;

import java.util.List;

import com.bbva.czic.account.business.dto.DTOIntAccount;
import com.bbva.czic.dto.canonical_model.OptionsList;

public interface ISrvIntAccount {
	
	public OptionsList addAccountBlocking(String idAccount, DTOIntAccount infoAccount);
	public void setConditionItem(String idAccount, DTOIntAccount infoAccount);
	public List<Object> listAccountIndicators(String indicatorId,
			String paginationKey, String pageSize);

}