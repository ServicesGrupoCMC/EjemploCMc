package com.bbva.czic.account.business.dto;

import java.util.List;


public class DTOIntContract {

	private String id;
	//Novatec - Inicio - SP15 - 13012016 - Se adiciona atributo
	private List<DTOIntCondition> conditions;
	//Novatec - Inicio - SP15 - 13012016 - Se adiciona atributo

	public DTOIntContract() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public List<DTOIntCondition> getConditions() {
        return conditions;
    }

    public void setConditions(List<DTOIntCondition> conditions) {
        this.conditions = conditions;
    }
}
