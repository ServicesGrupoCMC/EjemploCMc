package com.bbva.czic.account.business.dto;

import java.util.List;

public class DTOIntCondition {
	
	private List<DTOIntItemCondition> items;

    public DTOIntCondition() {
        
    }

    public List<DTOIntItemCondition> getItems() {
        return items;
    }

    public void setItems(List<DTOIntItemCondition> items) {
        this.items = items;
    }

}
