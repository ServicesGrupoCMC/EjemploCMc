package com.bbva.czic.account.business.dto;

import java.util.List;

public class DTOIntItemCondition {
	
	private String name;
	private List<DTOIntItemConditionValue> values;
	
	public DTOIntItemCondition(){
		
	}
	
	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DTOIntItemConditionValue> getValues() {
        return values;
    }

    public void setValues(List<DTOIntItemConditionValue> values) {
        this.values = values;
    }

}
