package com.bbva.czic.account.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.czic.account.business.ISrvIntAccount;
import com.bbva.czic.account.business.dto.DTOIntAccount;
import com.bbva.czic.account.dao.AccountDAO;
import com.bbva.czic.dto.canonical_model.OptionsList;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

@Service
public class SrvIntAccount implements ISrvIntAccount {

	private static I18nLog log = I18nLogFactory.getLogI18n(SrvIntAccount.class, "META-INF/spring/i18n/log/mensajesLog");

	@Autowired
	BusinessServicesToolKit bussinesToolKit;

	@Autowired
	AccountDAO accountDAO;

	@Override
	public OptionsList addAccountBlocking(String idAccount, DTOIntAccount infoAccount) {
		return accountDAO.addAccountBlocking(idAccount, infoAccount);
	}

	@Override
	public void setConditionItem(String idAccount, DTOIntAccount infoAccount) {
		accountDAO.setConditionItem(idAccount, infoAccount);		
	}
	
	@Override
	public List<Object> listAccountIndicators(String indicatorId, String paginationKey, String pageSize) {
		return accountDAO.listAccountIndicators(indicatorId, paginationKey, pageSize);		
	}
}
