package com.bbva.czic.account.business.dto;

public class DTOIntItemConditionValue {
	
	private String value;
	
	public DTOIntItemConditionValue(){
		
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}