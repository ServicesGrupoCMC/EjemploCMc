package com.bbva.czic.account.business.dto;

import java.util.Calendar;

import com.bbva.jee.arq.spring.core.servicing.utils.Money;

public class DTOIntAccountBlocking {

	public final static long serialVersionUID = 1L;
    private String checkbookId;
    private Calendar creationDate;
    private DTOIntOptionsList type;
    private String description;
    private Money blockedAmount;
    private Calendar startDate;
    private Calendar endDate;
    private String concept;
    private String commercialFunction;
    private String checkId;

    public DTOIntAccountBlocking() {
        //default constructor
    }

    public String getCheckbookId() {
        return checkbookId;
    }

    public void setCheckbookId(String checkbookId) {
        this.checkbookId = checkbookId;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public DTOIntOptionsList getType() {
        return type;
    }

    public void setType(DTOIntOptionsList type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Money getBlockedAmount() {
        return blockedAmount;
    }

    public void setBlockedAmount(Money blockedAmount) {
        this.blockedAmount = blockedAmount;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getCommercialFunction() {
        return commercialFunction;
    }

    public void setCommercialFunction(String commercialFunction) {
        this.commercialFunction = commercialFunction;
    }

	public void setCheckId(String checkId) {
		this.checkId = checkId;
	}

	public String getCheckId() {
		return checkId;
	}
	
}
