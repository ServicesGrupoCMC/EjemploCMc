package com.bbva.czic.account.dao.model.ozov;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>OZOV</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionOzov</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionOzov</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: COBD.CN.TMP.QGDTCCT.OZOZOV.cct
 * OZOVBLOQUEO DE CHEQUES Y CHEQUERAS     OZ        OZ1COZOVBVDKNPO OZECOVE0            OZOV  NS3000CNNNNN    SSTN    C   SNNSSNNN  NN                2015-09-29CE37479 2015-09-2913.23.18CE37479 2015-09-29-13.21.05.932905CE37479 0001-01-010001-01-01
 * 
 * FICHERO: COBD.CN.TMP.QGDTFDF.OZECOVE0.fdf
 * OZECOVE0�E - BLOQUEO CHEQUES Y CHEQUERA�F�06�00068�01�00001�CUENTA �CUENTA DEL CLIENTE  �A�020�0�R�        �
 * OZECOVE0�E - BLOQUEO CHEQUES Y CHEQUERA�F�06�00068�02�00021�NUMCHEQ�NUMERO DE CHEQUE    �N�009�0�O�        �
 * OZECOVE0�E - BLOQUEO CHEQUES Y CHEQUERA�F�06�00068�03�00030�PRICHEQ�NUMERO PRIMER CHEQUE�N�009�0�O�        �
 * OZECOVE0�E - BLOQUEO CHEQUES Y CHEQUERA�F�06�00068�04�00039�ULTCHEQ�NUMERO ULTIMO CHEQUE�N�009�0�O�        �
 * OZECOVE0�E - BLOQUEO CHEQUES Y CHEQUERA�F�06�00068�05�00048�TIPBLOQ�MOTIVO DEL BLOQUEO  �A�001�0�R�        �
 * OZECOVE0�E - BLOQUEO CHEQUES Y CHEQUERA�F�06�00068�06�00049�OBSERVA�COMENTARIOS         �A�020�0�O�        �
 * 
 * FICHERO: COBD.CN.TMP.QGDTFDF.OZECOVS0.fdf
 * OZECOVS0�S - BLOQUEO CHEQUES Y CHEQUERA�X�02�00019�01�00001�FECHOPE�FECHA DE OPERACION  �A�010�0�S�        �
 * OZECOVS0�S - BLOQUEO CHEQUES Y CHEQUERA�X�02�00019�02�00011�NUMOPER�NUMERO DE OPERACION �N�009�0�S�        �
 * 
 * FICHERO: COBD.CN.TMP.QGDTFDX.OZOZOV.fdx
 * OZOVOZECOVS0COPY    OZ1COZOV1S                             CE37479 2015-09-29-13.35.29.603413CE37479 2015-09-29-13.35.29.603459
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionOzov
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "OZOV",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionOzov.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoOZECOVE0.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionOzov implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}