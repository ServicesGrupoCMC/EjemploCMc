package com.bbva.czic.account.dao.model.ozqd;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>OZECQDE0</code> de la transacci&oacute;n <code>OZQD</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZECQDE0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZECQDE0 {

	/**
	 * <p>Campo <code>IDCTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String idcta;
	
	/**
	 * <p>Campo <code>ACCEPT</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "ACCEPT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String accept;
	
}