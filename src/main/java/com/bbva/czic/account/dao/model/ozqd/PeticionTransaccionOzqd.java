package com.bbva.czic.account.dao.model.ozqd;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>OZQD</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionOzqd</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionOzqd</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: COBD.CN.TMP.QGDTCCT.OZOZQD.TXT
 * OZQDACEPTACION CONTRATO TERMI Y CONDICIOZ        OZ1COZQDBVDKNPO OZECQDE0            OZQD  NS3000NNNNNN    SSTN    C   SNNSSNNN  NN                2015-11-27CE37480 2015-11-2717.34.52CE37480 2015-11-27-17.34.41.229677CE37480 0001-01-010001-01-01
 * 
 * FICHERO: COBD.CN.TMP.QGDTFDF.OZECQDE0.TXT
 * OZECQDE0�ACEPTACION TERMINOS-CONDICIONE�F�02�00021�01�00001�IDCTA  �ID DE LA CUENTA     �A�020�0�R�        �
 * OZECQDE0�ACEPTACION TERMINOS-CONDICIONE�F�02�00021�02�00021�ACCEPT �INDICADOR ACEPTACION�A�001�0�R�        �
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionOzqd
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "OZQD",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionOzqd.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoOZECQDE0.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionOzqd implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}