package com.bbva.czic.account.dao.model.oz55;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>OZ551S</code> de la transacci&oacute;n <code>OZ55</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZ551S")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZ551S {
	
	/**
	 * <p>Campo <code>CUENTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cuenta;
	
}