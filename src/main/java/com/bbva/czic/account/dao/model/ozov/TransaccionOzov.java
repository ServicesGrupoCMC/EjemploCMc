package com.bbva.czic.account.dao.model.ozov;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>OZOV</code>
 * 
 * @see PeticionTransaccionOzov
 * @see RespuestaTransaccionOzov
 */
@Component
public class TransaccionOzov implements InvocadorTransaccion<PeticionTransaccionOzov,RespuestaTransaccionOzov> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionOzov invocar(PeticionTransaccionOzov transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionOzov.class, RespuestaTransaccionOzov.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionOzov invocarCache(PeticionTransaccionOzov transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionOzov.class, RespuestaTransaccionOzov.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}