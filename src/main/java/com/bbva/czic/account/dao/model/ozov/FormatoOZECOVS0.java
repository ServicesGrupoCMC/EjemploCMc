package com.bbva.czic.account.dao.model.ozov;

import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>OZECOVS0</code> de la transacci&oacute;n <code>OZOV</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZECOVS0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZECOVS0 {
	
	/**
	 * <p>Campo <code>FECHOPE</code>, &iacute;ndice: <code>1</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 1, nombre = "FECHOPE", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fechope;
	
	/**
	 * <p>Campo <code>NUMOPER</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 2, nombre = "NUMOPER", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
	private Integer numoper;
	
}