package com.bbva.czic.account.dao.model.oz55;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>OZ550S</code> de la transacci&oacute;n <code>OZ55</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZ550S")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZ550S {
	
	/**
	 * <p>Campo <code>INDPAG</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "INDPAG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indpag;
	
	/**
	 * <p>Campo <code>NUMPAG</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NUMPAG", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer numpag;
	
}