package com.bbva.czic.account.dao.model.oz55;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>OZ550E</code> de la transacci&oacute;n <code>OZ55</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZ550E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZ550E {

	/**
	 * <p>Campo <code>INDPAG</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "INDPAG", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer indpag;
	
}