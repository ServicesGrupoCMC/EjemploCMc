package com.bbva.czic.account.dao.model.ozov;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>OZECOVE0</code> de la transacci&oacute;n <code>OZOV</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZECOVE0")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZECOVE0 {

	/**
	 * <p>Campo <code>CUENTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String cuenta;
	
	/**
	 * <p>Campo <code>NUMCHEQ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 2, nombre = "NUMCHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
	private Integer numcheq;
	
	/**
	 * <p>Campo <code>PRICHEQ</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 3, nombre = "PRICHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
	private Integer pricheq;
	
	/**
	 * <p>Campo <code>ULTCHEQ</code>, &iacute;ndice: <code>4</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 4, nombre = "ULTCHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
	private Integer ultcheq;
	
	/**
	 * <p>Campo <code>TIPBLOQ</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "TIPBLOQ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipbloq;
	
	/**
	 * <p>Campo <code>OBSERVA</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "OBSERVA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String observa;
	
}