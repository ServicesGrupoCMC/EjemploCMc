package com.bbva.czic.account.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.bbva.czic.account.business.dto.DTOIntAccount;
import com.bbva.czic.account.dao.model.oz55.FormatoOZ550E;
import com.bbva.czic.account.dao.model.oz55.FormatoOZ550S;
import com.bbva.czic.account.dao.model.oz55.FormatoOZ551S;
import com.bbva.czic.account.dao.model.oz55.PeticionTransaccionOz55;
import com.bbva.czic.account.dao.model.oz55.RespuestaTransaccionOz55;
import com.bbva.czic.account.dao.model.oz55.TransaccionOz55;
import com.bbva.czic.account.dao.model.ozov.FormatoOZECOVE0;
import com.bbva.czic.account.dao.model.ozov.FormatoOZECOVS0;
import com.bbva.czic.account.dao.model.ozov.PeticionTransaccionOzov;
import com.bbva.czic.account.dao.model.ozov.RespuestaTransaccionOzov;
import com.bbva.czic.account.dao.model.ozov.TransaccionOzov;
import com.bbva.czic.account.dao.model.ozqd.FormatoOZECQDE0;
import com.bbva.czic.account.dao.model.ozqd.PeticionTransaccionOzqd;
import com.bbva.czic.account.dao.model.ozqd.RespuestaTransaccionOzqd;
import com.bbva.czic.account.dao.model.ozqd.TransaccionOzqd;
import com.bbva.czic.account.facade.v02.mapper.Mapper;
import com.bbva.czic.dto.canonical_model.OptionsList;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.ErrorMappingHelper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;
@Component
public class AccountDAOImpl  implements AccountDAO {
	
	private static I18nLog log = I18nLogFactory.getLogI18n(AccountDAOImpl.class,"META-INF/spring/i18n/log/mensajesLog");

	@Autowired
	private ErrorMappingHelper emh;
	@Autowired
	private TransaccionOzov ozov;
	@Autowired
	private TransaccionOzqd ozqd;
	@Autowired
	private TransaccionOz55 oz55;
	
	@Override
	public OptionsList addAccountBlocking(String idAccount, DTOIntAccount infoAccount) {
		OptionsList numope=new OptionsList();
		
		PeticionTransaccionOzov peticion = new PeticionTransaccionOzov();
		FormatoOZECOVE0 entrada = Mapper.formatoOZECOVE0_addAccountBlocking(idAccount, infoAccount);
		peticion.getCuerpo().getPartes().add(entrada);
		RespuestaTransaccionOzov res = ozov.invocar(peticion);
		BusinessServiceException exception = emh.toBusinessServiceException(res);
		if (exception != null) {
			throw exception;
		}
		CopySalida cs=res.getCuerpo().getParte(CopySalida.class);
		FormatoOZECOVS0 salida=Mapper.FormatoOZECOVS0_copySalida(cs);

		numope.setId("" + salida.getNumoper());
		return numope;
	}
	
	@Override
	public void setConditionItem(String idAccount, DTOIntAccount infoAccount) throws BusinessServiceException {
		
		
		PeticionTransaccionOzqd peticion = new PeticionTransaccionOzqd();
		FormatoOZECQDE0 entrada = Mapper.formatoOZECQDE0_setConditionItem(idAccount, infoAccount);    
		peticion.getCuerpo().getPartes().add(entrada);
		RespuestaTransaccionOzqd res = ozqd.invocar(peticion);
		BusinessServiceException exception = emh.toBusinessServiceException(res);
		if (exception != null) {
			throw exception;
		}
		
	}
	@Override
	public List<Object> listAccountIndicators(String indicatorId, String paginationKey, String pageSize) throws BusinessServiceException {
		PeticionTransaccionOz55 peticion = new PeticionTransaccionOz55();
		FormatoOZ550E formatoEntrada = new FormatoOZ550E();
		formatoEntrada.setIndpag(Integer.parseInt(pageSize));
		peticion.getCuerpo().getPartes().add(formatoEntrada);
		RespuestaTransaccionOz55 respuesta = oz55.invocar(peticion);
		
		BusinessServiceException exception = emh.toBusinessServiceException(respuesta);
		
		if (exception != null) {
			throw exception;
		}
		
		List<Object> listRespuesta = respuesta.getCuerpo().getPartes();
		List<CopySalida> listCopys= Mapper.mapResponseToCopy(listRespuesta);
		List<FormatoOZ550S> listFormatos= Mapper.mapCopyToFormatoOZ550S(listCopys);
		List<FormatoOZ551S> listFormatosSec= Mapper.mapCopyToFormatoOZ551S(listCopys);
		
		return Mapper.mapCopyToResponseObject(listFormatosSec, listFormatos);		
	}
}

