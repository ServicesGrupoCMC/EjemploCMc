package com.bbva.czic.account.dao.model.oz55;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>OZ55</code>
 * 
 * @see PeticionTransaccionOz55
 * @see RespuestaTransaccionOz55
 */
@Component
public class TransaccionOz55 implements InvocadorTransaccion<PeticionTransaccionOz55,RespuestaTransaccionOz55> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionOz55 invocar(PeticionTransaccionOz55 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionOz55.class, RespuestaTransaccionOz55.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionOz55 invocarCache(PeticionTransaccionOz55 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionOz55.class, RespuestaTransaccionOz55.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}