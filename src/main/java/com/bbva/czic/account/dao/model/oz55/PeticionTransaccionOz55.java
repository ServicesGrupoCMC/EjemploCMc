package com.bbva.czic.account.dao.model.oz55;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>OZ55</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionOz55</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionOz55</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: OZ55.CCT
 * OZ55CREACION TARJETA AMPAR             OZ        OZ1COZ55BVDKNPO OZ550E              OZ55  NS3000CNNNNN    SSTN    C   SNNSSNNN  NN                2015-02-26CE15505 2015-11-1112.17.22CICSDC112015-02-26-12.49.37.786590CE15505 0001-01-010001-01-01
 * FICHERO: OZ55.FDX
 * OZ55OZ550S  OZ550S  OZ1COZ551S                             CE30793 2015-10-15-12.10.06.650844CE30793 2015-10-15-12.10.06.650873
 * OZ55OZ551S  OZ551S  OZ1COZ551S                             CE30793 2015-10-15-12.10.06.650844CE30793 2015-10-15-12.10.06.650873
 * FICHERO: OZ550E.FDF
 * OZ550E  |E - ENTRADA LISTADOR DESEMBOLS|F|05|00059|01|00001|INDPAG |INDICADOR PAGINACION|A|001|0|R|        |
 * FICHERO: OZ550S.FDF
 * OZ550S  |S - SALIDA DESEMBOLSO         |X|01|00015|01|00001|INDPAG |INDICADOR PAGINACION|A|001|0|S|        |
 * OZ550S  |S - SALIDA DESEMBOLSO         |X|01|00015|02|00002|NUMPAG |NUMERO PAGINA       |A|002|0|S|        |
 * FICHERO: OZ551S.FDF
 * OZ551S  |S - SALIDA DESEMBOLSO         |X|01|00015|01|00001|CUENTA |NUMERO CUENTA ASOCIA|A|020|0|S|        |
</pre></code>
 * 
 * @see RespuestaTransaccionOz55
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "OZ55",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionOz55.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoOZ550E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionOz55 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}