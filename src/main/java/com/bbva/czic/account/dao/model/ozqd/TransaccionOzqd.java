package com.bbva.czic.account.dao.model.ozqd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>OZQD</code>
 * 
 * @see PeticionTransaccionOzqd
 * @see RespuestaTransaccionOzqd
 */
@Component
public class TransaccionOzqd implements InvocadorTransaccion<PeticionTransaccionOzqd,RespuestaTransaccionOzqd> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionOzqd invocar(PeticionTransaccionOzqd transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionOzqd.class, RespuestaTransaccionOzqd.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionOzqd invocarCache(PeticionTransaccionOzqd transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionOzqd.class, RespuestaTransaccionOzqd.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}