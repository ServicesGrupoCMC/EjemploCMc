package com.bbva.czic.account.dao;

import java.util.List;

import com.bbva.czic.account.business.dto.DTOIntAccount;
import com.bbva.czic.dto.canonical_model.OptionsList;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;

public interface AccountDAO {

	public OptionsList addAccountBlocking(String idAccount, DTOIntAccount infoAccount);
	public void setConditionItem(String idAccount, DTOIntAccount infoAccount)throws BusinessServiceException;
	public List<Object> listAccountIndicators(String indicatorId,String paginationKey, String pageSize) throws BusinessServiceException;

}
