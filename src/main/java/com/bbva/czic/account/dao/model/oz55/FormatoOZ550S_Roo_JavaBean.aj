// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.czic.account.dao.model.oz55;

import java.lang.String;

privileged aspect FormatoOZ550S_Roo_JavaBean {
    
    public String FormatoOZ550S.getIndpag() {
        return this.indpag;
    }
    
    public void FormatoOZ550S.setIndpag(String indpag) {
        this.indpag = indpag;
    }
    
    public Integer FormatoOZ550S.getNumpag() {
        return this.numpag;
    }
    
    public void FormatoOZ550S.setNumpag(Integer numpag) {
        this.numpag = numpag;
    }
    
}
