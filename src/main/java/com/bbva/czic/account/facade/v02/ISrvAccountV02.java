package com.bbva.czic.account.facade.v02;

import com.bbva.czic.dto.canonical_model.Account;
import com.bbva.czic.dto.canonical_model.OptionsList;

import javax.ws.rs.core.Response;

public interface ISrvAccountV02 {
	
	public OptionsList addAccountBlocking(String idAccount, Account infoAccount);
	public Response setConditionItem(String idAccount, Account infoAccount);
	public Response listAccountIndicators(String filter, String paginationKey, String pageSize);

}