package com.bbva.czic.account.facade.v02.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.bbva.czic.account.business.dto.DTOIntAccount;
import com.bbva.czic.account.business.dto.DTOIntAccountBlocking;
import com.bbva.czic.account.business.dto.DTOIntOptionsList;
import com.bbva.czic.account.dao.model.oz55.FormatoOZ550S;
import com.bbva.czic.account.dao.model.oz55.FormatoOZ551S;
import com.bbva.czic.account.dao.model.ozov.FormatoOZECOVE0;
import com.bbva.czic.account.dao.model.ozov.FormatoOZECOVS0;
import com.bbva.czic.account.dao.model.ozqd.FormatoOZECQDE0;
import com.bbva.czic.account.business.dto.DTOIntCondition;
import com.bbva.czic.account.business.dto.DTOIntItemCondition;
import com.bbva.czic.account.business.dto.DTOIntItemConditionValue;
import com.bbva.czic.dto.canonical_model.Account;
import com.bbva.czic.dto.canonical_model.AccountBlocking;
import com.bbva.czic.dto.canonical_model.Condition;
import com.bbva.czic.dto.canonical_model.ItemCondition;
import com.bbva.czic.dto.canonical_model.ItemConditionValue;
import com.bbva.czic.dto.canonical_model.OptionsList;
import com.bbva.czic.routine.utilities.rm.mappers.IFilterMapper;
import com.bbva.czic.routine.utilities.rm.mappers.impl.FilterMapper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;



public class Mapper {
/**
 * Grupo CMC SPRINT 18
 * @param filter
 * @param bussinesToolKit
 * @return
 */
public static String mapFilterAccount(String filter, BusinessServicesToolKit bussinesToolKit){
	IFilterMapper filterMapper = new FilterMapper();
	String indicatorId  = null;
	if(filter != null)
		indicatorId = filterMapper.getMapFilter("indicatorId", filter, bussinesToolKit);
	
	return indicatorId;
	
}
public static List<CopySalida> mapResponseToCopy(List<Object> listResponse){
	List<CopySalida> listResponseForm = new ArrayList<CopySalida>();
	for(Object responseObject : listResponse){
		CopySalida formatoSalida = new CopySalida();
		
		if((CopySalida)responseObject != null){
			formatoSalida =(CopySalida)responseObject;
			listResponseForm.add(formatoSalida);
		}
		
	}
	return listResponseForm;
}
public static List<FormatoOZ550S> mapCopyToFormatoOZ550S(List<CopySalida> copySalida){
	List<FormatoOZ550S> listOutputFormat = new ArrayList<FormatoOZ550S>();
	for(CopySalida copy : copySalida){
		if(copy.getCopy(FormatoOZ550S.class)!=null){
			listOutputFormat.add(copy.getCopy(FormatoOZ550S.class));
		}
	}
	return listOutputFormat;
}
public static List<FormatoOZ551S> mapCopyToFormatoOZ551S(List<CopySalida> copySalida){
	List<FormatoOZ551S> listOutputFormat = new ArrayList<FormatoOZ551S>();
	for(CopySalida copy : copySalida){
		if(copy.getCopy(FormatoOZ551S.class)!=null){
			listOutputFormat.add(copy.getCopy(FormatoOZ551S.class));
		}
	}
	return listOutputFormat;
}
public static List<Object> mapCopyToResponseObject(List<FormatoOZ551S> listFormatoOZ551S,List<FormatoOZ550S> listFormatoOZ550S){
	List<Object> response = new ArrayList<Object>();
	List<DTOIntAccount> listAccount = new ArrayList<DTOIntAccount>();
	for(FormatoOZ551S formatoOZ551S: listFormatoOZ551S){
		
		DTOIntAccount dtoIntAccount = new DTOIntAccount(); 
		dtoIntAccount.setId(formatoOZ551S.getCuenta());
		listAccount.add(dtoIntAccount);
	}
	response.add(listAccount);
	for(FormatoOZ550S formatoOZ550S: listFormatoOZ550S){
		response.add(formatoOZ550S.getIndpag());
		response.add(formatoOZ550S.getNumpag());
	}
	
	return response; 
	
}
/**
 * FIN GRUPO CMC SPRINT 18
 * 
 */
 public static OptionsList optionsListMap(DTOIntOptionsList dtoIntOptionsList) { 
 		OptionsList optionsList = new OptionsList(); 
 		BeanUtils.copyProperties(dtoIntOptionsList, optionsList); 
 		return optionsList; 
 } 

		
 public static DTOIntOptionsList dtoIntOptionsListMap(OptionsList optionsList) { 
 		DTOIntOptionsList dtoIntOptionsList = new DTOIntOptionsList(); 
 		BeanUtils.copyProperties(optionsList, dtoIntOptionsList); 
 		return dtoIntOptionsList; 
 } 

		
 public static AccountBlocking accountBlockMap(DTOIntAccountBlocking dtoIntAccountBlock) { 
 		AccountBlocking accountBlock = new AccountBlocking(); 
 		BeanUtils.copyProperties(dtoIntAccountBlock, accountBlock); 
 		return accountBlock; 
 } 

		
 public static DTOIntAccountBlocking dtoIntAccountBlockMap(AccountBlocking accountBlock) { 
 		DTOIntAccountBlocking dtoIntAccountBlock = new DTOIntAccountBlocking(); 
 		BeanUtils.copyProperties(accountBlock, dtoIntAccountBlock); 
 		return dtoIntAccountBlock; 
 } 

		
 public static Account accountMap(DTOIntAccount dtoIntAccount) { 
 		Account account = new Account(); 
 		BeanUtils.copyProperties(dtoIntAccount, account); 
 		return account; 
 } 

		
 public static DTOIntAccount dtoIntAccountMap(Account account) { 
 		DTOIntAccount dtoIntAccount = new DTOIntAccount(); 
 		BeanUtils.copyProperties(account, dtoIntAccount, new String[]{"blockings","checkbooks","conditions"});
 		List<DTOIntAccountBlocking> bloqueos = new ArrayList<DTOIntAccountBlocking>();
 		for(AccountBlocking ab: account.getBlockings()){
 			DTOIntAccountBlocking dtoAb = new DTOIntAccountBlocking();
 			BeanUtils.copyProperties(ab, dtoAb, new String[]{"type"});
 			DTOIntOptionsList type = new DTOIntOptionsList();
 			if(ab.getType() != null){
	 			BeanUtils.copyProperties(ab.getType(), type);
	 			dtoAb.setType(type);
 			}
 			bloqueos.add(dtoAb);
 		}
 		dtoIntAccount.setBlockings(bloqueos);
 		dtoIntAccount.setConditions(listDtoIntConditionMap(account.getConditions()));
 		return dtoIntAccount;
 }
 

 
 public static ItemCondition itemConditionMap(DTOIntItemCondition dtoIntItemCondition) { 
		ItemCondition itemCondition = new ItemCondition();
		BeanUtils.copyProperties(dtoIntItemCondition, itemCondition, new String[] {"values"}); 
		if(dtoIntItemCondition.getValues() != null)
			itemCondition.setValues(listItemConditionValueMap(dtoIntItemCondition.getValues()));
		
		return itemCondition; 
} 

		
public static DTOIntItemCondition dtoIntItemConditionMap(ItemCondition itemCondition) { 
		DTOIntItemCondition dtoIntItemCondition = new DTOIntItemCondition(); 
		BeanUtils.copyProperties(itemCondition, dtoIntItemCondition,  new String[] {"values"}); 
		dtoIntItemCondition.setValues(listDtoIntItemConditionValueMap(itemCondition.getValues()));
		
		return dtoIntItemCondition; 
} 

public static List<ItemConditionValue> listItemConditionValueMap (List<DTOIntItemConditionValue> list) {		
		List<ItemConditionValue> newList = new ArrayList<ItemConditionValue>();	
		for(DTOIntItemConditionValue item : list) {	
			newList.add(itemConditionValueMap(item));
		 }	
			
		 return newList;	
	 }		
	 		
	public static List<DTOIntItemConditionValue> listDtoIntItemConditionValueMap (List<ItemConditionValue> list) {		
		List<DTOIntItemConditionValue> newList = new ArrayList<DTOIntItemConditionValue>();	
		for(ItemConditionValue item : list) {	
			newList.add(dtoIntItemConditionValueMap(item));
		 }	
		 	
		 return newList;	
	 }
	
	public static ItemConditionValue itemConditionValueMap(DTOIntItemConditionValue dtoIntItemConditionValue) { 
 		ItemConditionValue itemConditionValue = new ItemConditionValue(); 
 		if(dtoIntItemConditionValue != null)
 			BeanUtils.copyProperties(dtoIntItemConditionValue, itemConditionValue); 
 		return itemConditionValue; 
 } 

		
 public static DTOIntItemConditionValue dtoIntItemConditionValueMap(ItemConditionValue itemConditionValue) { 
 		DTOIntItemConditionValue dtoIntItemConditionValue = new DTOIntItemConditionValue(); 
 		BeanUtils.copyProperties(itemConditionValue, dtoIntItemConditionValue); 
 		return dtoIntItemConditionValue; 
 }
 
 public static List<Condition> listConditionMap (List<DTOIntCondition> list) {		
		List<Condition> newList = new ArrayList<Condition>();	
		for(DTOIntCondition item : list) {	
			newList.add(conditionMap(item));
		 }	
			
		 return newList;		 
		 }		
	 		
public static List<DTOIntCondition> listDtoIntConditionMap (List<Condition> list) {		
	List<DTOIntCondition> newList = new ArrayList<DTOIntCondition>();	
	for(Condition item : list) {	
		newList.add(dtoIntConditionMap(item));
	 }	
	 	
	 return newList;	
}

public static Condition conditionMap(DTOIntCondition dtoIntCondition) { 
		Condition condition = new Condition(); 
		BeanUtils.copyProperties(dtoIntCondition, condition, new String[] {"items"});
		if(dtoIntCondition.getItems() != null)
			condition.setItems(listItemConditionMap(dtoIntCondition.getItems()));
		
		return condition; 
} 

	
public static DTOIntCondition dtoIntConditionMap(Condition condition) { 
		DTOIntCondition dtoIntCondition = new DTOIntCondition(); 
		BeanUtils.copyProperties(condition, dtoIntCondition, new String[] {"items"}); 
		dtoIntCondition.setItems(listDtoIntItemConditionMap(condition.getItems()));
		
		return dtoIntCondition; 
}

public static List<ItemCondition> listItemConditionMap (List<DTOIntItemCondition> list) {		
	List<ItemCondition> newList = new ArrayList<ItemCondition>();	
	for(DTOIntItemCondition item : list) {	
		newList.add(itemConditionMap(item));
	}	

return newList;	
}		

public static List<DTOIntItemCondition> listDtoIntItemConditionMap (List<ItemCondition> list) {		
	List<DTOIntItemCondition> newList = new ArrayList<DTOIntItemCondition>();	
	for(ItemCondition item : list) {	
		newList.add(dtoIntItemConditionMap(item));
	}	

return newList;	
}


public static FormatoOZECOVE0 formatoOZECOVE0_addAccountBlocking(String idAccount, DTOIntAccount infoAccount) {
	
	FormatoOZECOVE0 entrada=new FormatoOZECOVE0();
	int idChequeFinal=0,idChequeInicial=0;
	
	entrada.setCuenta(idAccount);
	idChequeInicial =Integer.parseInt(infoAccount.getBlockings()!=null&&infoAccount.getBlockings().get(0)!=null?infoAccount.getBlockings().get(0).getCheckId():null);
	if(infoAccount.getBlockings()!=null&&infoAccount.getBlockings().size()>1){
		idChequeFinal=Integer.parseInt(infoAccount.getBlockings()!=null&&infoAccount.getBlockings().get(1)!=null?infoAccount.getBlockings().get(1).getCheckId():"0");
	}
	entrada.setNumcheq(idChequeInicial);
	entrada.setObserva(infoAccount.getBlockings().get(0).getDescription());
	entrada.setTipbloq(infoAccount.getBlockings().get(0).getType().getId());
	if(idChequeFinal!=0){
		entrada.setPricheq(idChequeInicial);
		entrada.setUltcheq(idChequeFinal);
	}
	return entrada;
}


public static FormatoOZECOVS0 FormatoOZECOVS0_copySalida(CopySalida cs) {
	FormatoOZECOVS0 salida=cs.getCopy(FormatoOZECOVS0.class);
	
	if(salida != null){
		return salida;
	}
	return null;
} 

public static  FormatoOZECQDE0 formatoOZECQDE0_setConditionItem(String idAccount, DTOIntAccount infoAccount){
	FormatoOZECQDE0 formato = new FormatoOZECQDE0();
	formato.setIdcta(idAccount);
	formato.setAccept(infoAccount.getConditions().get(0).getItems().get(0).getValues().get(0).getValue());
	return formato;
	}

	
	
}

